<?php

use HireMe\Repositories\CategoryRepo;
use HireMe\Repositories\CandidateRepo;

class CandidatesController extends BaseController {

	/*
	 * Esto es inyeccion de dependencias, el caso es que te llega el repositorio como parametro ya instanciado
	 */

	protected $categoryRepo;
	protected $candidateRepo;

	public function __construct(CategoryRepo $categoryRepo,
								CandidateRepo $candidateRepo)
	{
		$this->categoryRepo = $categoryRepo;
		$this->candidateRepo = $candidateRepo;
	}

	public function category($slug, $id)
	{
		/*
		 * La manera correcta de hacer consultas es a travz de los repositorios, por ello
		 * No me sirve hacer esto
				$category = Category::find($id);
		 */

		/*
		 * Como estamos trabajando con un repositorio y no directamente con eloquent, 
		 * imagina que quieres cambiar eloquent por otra cosa, el controller funcionaria con lo que sea
		 * siempre y cuando le devuelvas lo que debes en cada metodo del repositorio :D
		 */

		$category = $this->categoryRepo->find($id);

		/*
		 * Por seguridad, es posible que el ID que me llega o el slug sea erroneo, 
		 * y no saque nada en la consulta o marque error, para eso debo manejar errores 404
		 */
		if( ! $category ) App::abort(404);

		return View::make('candidates/category', compact('category'));
	}

	public function show($slug, $id){
		$candidate = $this->candidateRepo->find($id);

		/*
		 * Por seguridad, es posible que el ID que me llega o el slug sea erroneo, 
		 * y no saque nada en la consulta o marque error, para eso debo manejar errores 404,
		 * Pero aqui uso una funcion que implemente en el controlador BASE...
		 */
		$this->notFoundUnless($candidate);

		return View::make('candidates/show', compact('candidate'));
	}

}
