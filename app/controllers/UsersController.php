<?php

use HireMe\Entities\User;
use HireMe\Managers\RegisterManager;
use HireMe\Managers\AccountManager;
use HireMe\Managers\ProfileManager;
use HireMe\Repositories\CandidateRepo;
use HireMe\Repositories\CategoryRepo;

class UsersController extends BaseController {

	protected $candidateRepo;
	protected $categoryRepo;

	/*
	 * Estamos inyectando dependencias para usarlas en los metodos
	 */
	public function __construct(CandidateRepo $candidateRepo,
								CategoryRepo $categoryRepo)
	{
		$this->candidateRepo = $candidateRepo;
		$this->categoryRepo = $categoryRepo;
	}

	public function signUp()
	{
		/*
		 * Instanciamos la clase para construir campos, 
		 * y lo pasamos a la vista para que lo use
			$fieldBuilder = new \HireMe\Components\FieldBuilder();
			return View::make('users/sign-up', compact('fieldBuilder'));
		 */

		/*
		 * Actualmente no hace falta instanciar mi clase Field porque ya tiene su service provider
		 * y como ya forma parte del nucleo de laravel la puedo usar donde sea
		 */
		return View::make('users/sign-up');
	}

	public function register()
	{
		$user = $this->candidateRepo->newCandidate();
		$manager = new RegisterManager($user, Input::all());
		$manager->save();

		return Redirect::route('home');

		//return Redirect::back()->withInput()->withErrors($manager->getErrors());

	}

	public function account()
	{
		$user = Auth::user();
		return View::make('users/account', compact('user'));
	}

	public function updateAccount()
	{
		/*
		 * Aqui ya no se crea un nuevo cantidato, sino que se trabaja con el que ya esta registrado para editar sus datos
		 */
		$user = Auth::user();
		$manager = new AccountManager($user, Input::all());

		/*
		 * En este caso, intentamos guardar, ya el baseManager sabe que hacer, pero aqui se trabaja con condiciones y no con exceptions
		 
		if($manager->save()){
			return Redirect::route('home');
		}

		*/

		/*
 		 * Cuando se trabaja con exceptions solo se debe hacer esto
		 */

		$manager->save();

		return Redirect::route('home');


		//return Redirect::back()->withInput()->withErrors($manager->getErrors());

	}

	public function profile()
	{
		$user 		= Auth::user();
		/*
		 * Al principio cuando me acabo de registrar, el candidato no existe, seria null
		 * Para corregir eso, se implementa en la Entidad el metodo getCandidate
		 */
		$candidate 	= $user->getCandidate();
		$categories = $this->categoryRepo->getList();
		$job_types 	= \Lang::get('utils.job_types');

		return View::make('users/profile', compact('candidate', 'categories', 'job_types'));
	}

	public function updateProfile()
	{
		$user 		= Auth::user();
		$candidate 	= $user->getCandidate();
		$manager 	= new ProfileManager($candidate, Input::all());
		$manager->save();
		
		return Redirect::route('home');

	}

}
