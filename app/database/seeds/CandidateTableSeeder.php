<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;
use HireMe\Entities\Candidate;
use HireMe\Entities\User;

class CandidateTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 30) as $index)
		{
			$fullName = $faker->name;

			//aqui user tiene los datos del usuario que fue creado :D para tener su ID y eso
			$user = User::create([
				'full_name' => $fullName,
				'email'		=> $faker->email,
				'password'	=> 1234,//\Hash::make(123456),
				'type'		=> 'candidate'
			]);

			Candidate::create([
				'id'			=> 	$user->id,
				'website'		=> 	$faker->url,
				'description'	=> 	$faker->text(200),
				'job_type'		=> 	$faker->randomElement(['full', 'partial', 'freelance']),
				'category_id'	=> 	$faker->randomElement([1,2,3]),
				'available'		=> 	true,
				'slug'			=> 	\Str::slug($fullName)
			]);
		}
	}

}