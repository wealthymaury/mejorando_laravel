@extends('layout')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-6">

				<h1>Sign Up</h1>

				{{ Form::open(['route' => 'register', 'method' => 'POST', 'role' => 'form', 'novalidate']) }}

					{{--  

						Esta es la manera tradicional de construid campos con laravel, la otra es la manera PRO

						<div class="form-group">
							{{ Form::label('full_name', 'Nombre completo') }}
							{{ Form::text('full_name', null, ['class' => 'form-control']) }}
							{{ $errors->first('full_name', '<p class="error_message">:message</p>') }}
						</div>

						<div class="form-group">
							{{ Form::label('email', 'Correo') }}
							{{ Form::email('email', null, ['class' => 'form-control']) }}
							{{ $errors->first('email', '<p class="error_message">:message</p>') }}
						</div>

						<div class="form-group">
							{{ Form::label('password', 'Clave') }}
							{{ Form::password('password', ['class' => 'form-control']) }}
							{{ $errors->first('password', '<p class="error_message">:message</p>') }}
						</div>

						<div class="form-group">
							{{ Form::label('password_confirmation', 'Repite tu clave') }}
							{{ Form::password('password_confirmation', ['class' => 'form-control']) }}
							{{ $errors->first('password_confirmation', '<p class="error_message">:message</p>') }}
						</div>

					--}}

					{{--  
						Esto funciona para cuando no tienes tu ServiceProvider,
						Aqui le pasas a la vista la variable $fieldBuilder desde el controller para que la use

						{{ $fieldBuilder->input('text', 'full_name') }}
					--}}

					{{-- 
						Cuando usas ServideProvider es asi 
						{{ App::make('field')->input('text', 'full_name') }}

						pero App::make es algo feo, como lo podemos hacer mejor?
						Hay qu usar los Facades, teniendo el Facade sin un alias se usaria asi, pero aun es feo
						{{ HireMe\Components\Field::input('text', 'full_name') }}

						agregando el Facade a los alias ya se puede usar bonito
						{{ Field::input('text', 'full_name') }}

						Aun hay una manera mas Bonita, para ellohay un truco
						puedes usar la funcion _call en FieldBuilder, para usar metodos que no existen como text
						{{ Field::text('full_name') }}
					--}}

					{{-- La mejor forma de hacerlo --}}
					{{ Field::text('full_name') }}
					{{ Field::email('email') }}
					{{ Field::password('password') }}
					{{ Field::password('password_confirmation' , ['placeholder' => 'repite tu clave']) }}
					

					<input type="submit" value="Registrar" class="btn btn-success">
				{{ Form::close() }}

			</div>
		</div>
	</div>
@stop