<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

/*
 * Esta ruta se manda llamar asi
	pubic/candidates/backend-developers/1
 */
Route::get('candidates/{slug}/{id}', ['as' => 'category', 'uses' => 'CandidatesController@Category']);

/*
 * Esta ruta se manda llamar asi
	pubic/duilio-palacios/1
 */
Route::get('{slug}/{id}', ['as' => 'candidate', 'uses' => 'CandidatesController@Show']);

/*
 * Rutas visibles solo para usuarios NO logueados
 */
Route::group(['before' => 'guest'], function(){
	/*
	 * Esta ruta se manda llamar asi
		pubic/sign-up
	 */
	Route::get('sing-up', ['as' => 'sign_up', 'uses' => 'UsersController@SignUp']);
	Route::post('sign-up', ['as' => 'register', 'uses' => 'UsersController@Register']);

	/*
	 * Rutas para control de sesion
	 */
	Route::post('login', ['as' => 'login', 'uses' => 'AuthController@login']);

});

/*
 * Rutas visibles solo para usuarios logueados
 */
Route::group(['before' => 'auth'], function(){

	require ( __DIR__ . '/routes/auth.php');
	
	/*
	 * Rutas para administrador
	 */
	Route::group(['before' => 'admin'], function(){
		
		require ( __DIR__ . '/routes/admin.php');

	});
});







