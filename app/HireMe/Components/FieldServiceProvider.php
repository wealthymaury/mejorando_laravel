<?php namespace HireMe\Components;

/*
 * Esta clase sirve para integrar mi clase FieldsBuilder al nucleo de laravel
 */

use Illuminate\Support\ServiceProvider;

class FieldServiceProvider extends ServiceProvider{

	/*
	 * ServiceProvider es una clase abstracta que obliga a tener el metodo register
	 */

	public function register()
	{
		/*
		 * app es un objeto de ServideProvider, pero gracias a php puedo agregarle atributos como si fuera un array
		 */

		$this->app['field'] = $this->app->share(function($app)
		{
			$fieldBuilder = new FieldBuilder(
				$app['form'], 
				$app['view'], 
				$app['session.store'],
				$app['translator'],
				$app['files']
			);

			return $fieldBuilder;
		});
	}
}