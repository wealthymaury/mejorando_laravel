<?php namespace HireMe\Components;

use Illuminate\Html\FormBuilder as Form;
use Illuminate\View\Factory as View;
use Illuminate\Session\Store as Session;
use Illuminate\Translation\Translator as Lang;
use Illuminate\Filesystem\Filesystem as File;

class FieldBuilder {

	protected $form;
	protected $view;
	protected $session;

	protected $defaultClass  = [
		'default'	=> 	'form-control',
		'checkbox'	=> 	''
	];

	/*
	 * Trabajar con inyeccion de dependencias es lo ideal en tus clases, en vez de usar los Facade
	 * Usar facade es Session::get
	 * User dependencias es $this->session->get
	 */
	public function __construct(Form $form, View $view, Session $session, Lang $lang, File $file)
	{
		$this->form = $form;
		$this->view = $view;
		$this->session = $session;
		$this->lang = $lang;
		$this->file = $file;
	}

	public function getDefaultClass($type)
	{
		/*
		 * Si tengo definido una clase para el tipo de campo la regreso, si no regreso la default
		 */

		if( isset($this->defaultClass[$type]) )
		{
			return $this->defaultClass[$type];
		}
		return $this->defaultClass['default'];
	}

	/*
	 * Cuando pones un '&' antes de una variable es para indicar que el paso de parametros es por referencia
	 */
	public function buildCssClasses($type, &$attributes)
	{
		$defaultClasses = $this->getDefaultClass($type);

		if( isset($attributes['class']) )
		{
			$attributes['class'] .= ' ' . $defaultClasses;
		}else
		{
			$attributes['class'] = $defaultClasses;
		}
	}

	public function buildLabel($name)
	{
		/* 
		 * Si el nombre del campo esta definido en Lang, pues lo podemos usar como label,
		 * Si no esta, genero un label con el nombre del campo
		 */
		if($this->lang->has('validation.attributes.' . $name))
		{
			$label = $this->lang->get('validation.attributes.' . $name);
		}
		else
		{
			$label = str_replace('_', ' ', $name);
		}

		/*
		 * La priemr letra mayuscula
		 */
		return ucfirst($label);
	}

	/*
	 * Construye el control y lo regresa para asignarlo a la vista
	 */
	public function buildControl($type, $name, $value = null, $attributes = array(), $options = array())
	{
		switch($type)
		{
			case 'select':
				$options = array('' => 'Seleccione...') + $options;
				return $this->form->select($name, $options, $value, $attributes);
			case 'password':
				return $this->form->password($name, $attributes);
			case 'checkbox':
				return $this->form->checkbox($name);
			case 'textarea':
				return $this->form->textarea($name, $value, $attributes);
			default:
				return $this->form->input($type, $name, $value, $attributes);
		}
	}

	public function buildError($name)
	{
		$error = null;
		/*
		 * Podemos checar el la session de laravel para ver si hay algun error para ese campo
		 */

		if($this->session->has('errors'))
		{
			$errors = $this->session->get('errors');

			if($errors->has($name))
			{
				$error = $errors->first($name);
			}
		}

		return $error;
	}

	public function buildTemplate($type)
	{
		/*
		 * Primero checamos que la plantilla exista, 
		 * retornamos la ubicacion de la plantilla segun el caso
		 */
		if($this->file->exists('app/views/fields/' . $type . '.blade.php'))
		{
			return 'fields/'. $type;
		}

		return 'fields/default';
	}

	/*
	 Las variables que se inicializan en los parametros son para indicar que el campo es opcional
	 */
	public function input($type, $name, $value = null, $attributes = array(), $options = array())
	{
		$this->buildCssClasses($type, $attributes);

		$label 		= $this->buildLabel($name);
		$control 	= $this->buildControl($type, $name, $value, $attributes, $options);
		$error 		= $this->buildError($name);
		$template 	= $this->buildTemplate($type);
		/*
		 * Esto hace el render de la vista del campo, pero observa como le pasa el template como variable, al final es una cadena
		 */
		return $this->view->make($template, compact('name', 'label', 'control', 'error'));
	}

	/*
	 * Ojo con esta funcion, se manda llamar cuando intentas llamar un metodo que no existe de la clase,
	 * Por ejemplo si quieres usar el metodo 'text' como lo hace laravel Form::text, con esto lo podemos hacer
	 */
	public function __call($method, $params)
	{
		/*
		 * Con este metodo agregamos un valor a la primer posicion de un array
		 */
		array_unshift($params, $method);

		/*
		 * Con esta linea llamamos indirectamente al metodo input de esta clase, es como hacer
		 		$this->input($params)
		 */
		return call_user_func_array([$this, 'input'], $params);
	}

	/*
	 * Para metodos que no siguen el mismo patron, es decir, que sus parametros cambian de orden, 
	 * podemos definir los metodos, Es para reordenar los parametros
	 */
	public function password($name, $attributes = array())
	{
		return $this->input('password', $name, null, $attributes);
	}

	public function select($name, $options, $value = null, $attributes = array())
	{
		return $this->input('select', $name, $value, $attributes, $options);
	}
}