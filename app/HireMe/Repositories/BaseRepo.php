<?php namespace HireMe\Repositories;

use HireMe\Entities\Category;

/*
 * Una clase abstracta es aquella que nunca la vas a usar directamente, 
 * sino que siempre la usaras para extenderla
 */

abstract class BaseRepo{

	protected $model;

	public function __construct(){
		$this->model = $this->getModel();
	}

	/*
	 * Esto es perron porque obligas a los repositorios que extiendan de esta clase,
	 * a que a wevo declaren su metodo getModel, y de esta manera lo puedes usar en el
	 * constructor y ademas te queda generico el asunto bien chido :D
	 */
	abstract public function getModel(); 

	public function find($id){
		return $this->model->find($id);
	}

}
