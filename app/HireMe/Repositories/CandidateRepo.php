<?php namespace HireMe\Repositories;

use HireMe\Entities\Candidate;
use HireMe\Entities\Category;
use HireMe\Entities\User;

/*
 * BaseRepo obliga a que las clases que la extiendan tengan un metodo a wevo, getModel
 */
class CandidateRepo extends BaseRepo{

	public function getModel(){
		return new Candidate;
	}

	public function findLatest($take = 10){
		/*
		 * Esto lo hace asi, para que de una vez se traega toda la info en una consulta
		 * y no estar haciendo consultas por cada iteracion en el ciclo :D,
		 * es como decir, dame las categorias, con los candidatos y de una vez con su usuario
		 */

		return Category::with([
			'candidates' => function($q) use ($take){
				$q->take($take);
				$q->orderBy('created_at', 'DESC');
			}, 
			'candidates.user'
		])->get();
	}

	public function newCandidate(){
		$user = new User();
		$user->type = 'candidate';

		return $user;
	}

}
