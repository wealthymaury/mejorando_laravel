<?php namespace HireMe\Repositories;

use HireMe\Entities\Category;

/*
 * BaseRepo obliga a que las clases que la extiendan tengan un metodo a wevo, getModel
 */
class CategoryRepo extends BaseRepo{

	public function getModel()
	{
		return new Category;
	}

	public function getList()
	{
		return Category::lists('name', 'id');
	}

}
