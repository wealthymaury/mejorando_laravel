<?php namespace HireMe\Entities;

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends \Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	/*
	 * El fillable es para decirle a laravel cuales campos pueden ser guardados de forma masiva,
	 * es decir, cuales se pondran guardar pasandose como array al modelo con User::create($data)
	 */
	protected $fillable = array('full_name', 'email', 'password');

	/*
	 * Definiendo un accesor, como las propiedades virtuales, es que puedes encriptar la contraseña
	 */
	public function setPasswordAttribute($value)
	{
		if( ! empty($value) )
		{
			$this->attributes['password'] = \Hash::make($value);
		}
	}

	/*
	 * Esto lo puse porque Eloquent no lo hizo automatico, no se porque :C
	 *
	public function type($value){
		$this->attributes['type'] = $value;
	}*/

	public function candidate()
	{
		return $this->hasOne('HireMe\Entities\Candidate', 'id', 'id');
	}

	public function getCandidate()
	{
		$candidate = $this->candidate;

		if(is_null($candidate)){
			$candidate = new Candidate();
			/*
			 * El nuevo candidato tiene que estar relacionado con este ususario
			 */
			$candidate->id = $this->id;
		}

		return $candidate;
	}

}
