<?php namespace HireMe\Entities;

class Category extends \Eloquent {
	protected $fillable = [];

	/*
	 * esto espa chingon, si te vas a views/candidates/category
	 * veras un foreach que intenta sacar los candidatos de una categoria
	 * dirias que como le hace puesto que no haces una consulta ni nada
	 * la respuesta es que ejecutas este metodo, y este sabe que hay una relacion 
	 * asi que no ocupas nada mas, :D
	 */
	
	public function candidates(){
		return $this->hasMany('HireMe\Entities\Candidate');
	} 

	/*
	 * Esto lo usa como atributo virtual para paginar la informacion
	 */
	public function getPaginateCandidatesAttribute(){
		return Candidate::with('user')->where('category_id', $this->id)->paginate();
	}
}