<?php namespace HireMe\Entities;

class Candidate extends \Eloquent {
	protected $fillable = ['website', 'description', 'job_type', 'category_id', 'available', 'slug'];

	/*
 	 * Para cuando paginemos salgan de 3 en 3, no hace nada mas
	 */
	protected $perPage = 3;

	/* 
	 * un candidato tiene un usuario, asi que :D,
	 * que chingon esta esto, dentro del foreach de  views/candidates/category,
	 * tienes que necesitas el usuario de un candidato, y para que te lo devuelva
	 * pues hay que generar la relacion :D
	 * esta relacion agrega 2 parametros, 1d, id que son los campos que relacionnan Candidato con Usuario
	 */
	
	public function user(){
		return $this->hasOne('HireMe\Entities\User', 'id', 'id');
	}

	/*
	 * Asi como sabe que una categoria tiene muchos candidatos, hay que definir la contraparte
	 * por ello un candidato pertenece a una categoria :D
	 */

	public function category(){
		return $this->belongsTo('HireMe\Entities\Category');
	}

	/*
	 * Esto es para generar un atributo virtual, que lo puedes usar como si estuviera en la tabla 
	 * pero en realidad no existe ahi :D
	 * Lo usas asi:
	   		{{ $candidate->job_type_title }}
	 */
	public function getjobTypeTitleAttribute(){
		/* 
		 * Esto tambien esta perron, porque puedes retornar algo traducido,
		 * y pues tu mismo generas las traducciones...
		 * Para que funcione debes crear el archivo llamado utils.php en la carpeta lang
		 * y ahi meter un array que se llame job_types 
		 */
		return \Lang::get('utils.job_types.'.$this->job_type);
	}
}