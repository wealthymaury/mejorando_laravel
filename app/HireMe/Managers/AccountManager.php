<?php namespace HireMe\Managers;

class AccountManager extends BaseManager{

	public function getRules(){
		$rules = [
			'full_name' => 'required',
			'email'		=> 'required|email|unique:users,email,' . $this->entity->id,
			'password'	=> 'confirmed',
			'password_confirmation'	=> ''
		];

		return $rules;
	}

	public function prepareData($data){
		/*
		 * Esta seria una proteccion para evitar que escriban etiquetas HTML en algun campo
		 */
		$data['full_name'] = strip_tags($data['full_name']);

		return $data;
	}
}