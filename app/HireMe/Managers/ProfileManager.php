<?php namespace HireMe\Managers;

class ProfileManager extends BaseManager{

	public function getRules(){
		$rules = [
			'website' 		=> 'required|url',
			'description'	=> 'required|max:100',
			'job_type'		=> 'required|in:full,partial,freelance', //puede ser valida cualquiera de las tres opciones
			'category_id'	=> 'required|exists:categories,id', //Que el calor exista en la tabla categorias en el campo id
			'available'		=> 'in:1,0' //el valor puede ser 1 o 0
		];

		return $rules;
	}

	public function prepareData($data)
	{
		/*
		 * En caso de que 'available' no llegue, yo le pongo el 0 para que se guarde
		 */
		if(! isset($data['available']))
		{
			$data['available'] = 0;
		}

		/*
		 * Aqui podemos asignar el slug para la ruta, el cual yo lo use en asignacion masiva
		 * pero tambien puedes asignarlo asi sin necesidad de agregarlo a la asignacion masiva
		 		$this->entity->slug = \Str::slug($this->entity->user->full_name);
		 */

		$data['slug'] = \Str::slug($this->entity->user->full_name);

		return $data;
	}
}