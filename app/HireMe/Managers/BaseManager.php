<?php namespace HireMe\Managers;

abstract class BaseManager{

	protected $entity;
	protected $data;
	//protected $errors;

	public function __construct($entity, $data){
		$this->entity 	= $entity;
		/*
		 * array_only sirve para que solo obtener los datos que se pasan en el segundo argumento, 
		 * es decir, los que se estan validando,
		 * en pocas palabras, aqui se filtra la data
		 */
		$this->data 	= array_only($data, array_keys($this->getRules())); 
	}

	/*
	 * Cada manager que extienda la clase tendria sus propias regras, 
	 * por eso el metodo sera declarado en cada clase
	 */
	abstract public function getRules();

	public function isValid(){
		$rules = $this->getRules();
		$validation = \Validator::make($this->data, $rules);

		/*
		 * Esta es la manera de manejar errores con condiciones

		$isValid = $validation->passes();
		$this->errors = $validation->messages();

		return $isValid;

		*/

		/*
		 * Esta es la manera de manejar errores con exceptions,
		 * ahora si falla no regresa un booleano, sino que lanza una exception 
		 */
		if($validation->fails())
		{
			throw new ValidationException('Validation failed', $validation->messages());
		}
	}

	/*
	 * Este metodo es usado para evitar cuando no se guarde el cambio de un check, 
	 * pues como no llega si lo desmarcas, pero si observas aqui no hace nada, 
	 * para usarlo hay que sobreescribirlo en el Manager hijo donde se ocupe
	 */
	public function prepareData($data)
	{
		return $data;
	}

	public function save(){
		/* 
		 * Usado antes de manejar exceptions

		if(!$this->isValid()){
			return false;
		}

		*/

		/*
		 * Ahora como lanza una exception solo lo mando llamar sin condicion
		 */
		$this->isValid();

		/*
		 * Se usa fill, porque aqui le mandas toda la data, pero solito sabe lo que se debe guardar y lo que no
		 * en base a lo que la entidad tenga como fillable en su declaracion
		 */
		$this->entity->fill($this->prepareData($this->data));
		$this->entity->save();

		return true;
	}

	/*
	 * Esta fue la primer alternativa, pero los errores ahora se lanzan mediante un objeto de execption
	 
	public function getErrors(){
		return $this->errors;
	}
	 */

}