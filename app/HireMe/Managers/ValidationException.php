<?php namespace HireMe\Managers;

class ValidationException extends \Exception 
{	
	protected $errors;

	/*
	 * Necesito masarle unos atributos al momento de crear el objeto
	 */
	public function __construct($message, $errors)
	{
		$this->errors = $errors;
		/*
		 * Para que esto funcione necesito llamar al contructor de la clase padre "Exception",
		 * con el mensaje que es obligatorio
		 */
		parent::__construct($message);
	}

	public function getErrors(){
		return $this->errors;
	}
}